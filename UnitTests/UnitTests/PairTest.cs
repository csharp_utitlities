/*
    Csharp_Utilities - some useful C# utilities
    Copyright (C) 2008 Jonas Granqvist (jonas.granqvist@gmail.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
using System;
using NUnit.Framework;
using CollectionUtilities;

namespace UnitTests
{
	[TestFixture()]
	public class PairTest
	{
		[Test()]
		public void PairWorks()
		{
			Pair<string, int> pair = new Pair<string, int>("asdf", 42);
			Assert.AreEqual("asdf", pair.first);
			Assert.AreEqual(42, pair.second);
		}
	}
}
