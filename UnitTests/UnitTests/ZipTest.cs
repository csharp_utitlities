/*
    Csharp_Utilities - some useful C# utilities
    Copyright (C) 2008 Jonas Granqvist (jonas.granqvist@gmail.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;
using NUnit.Framework;
using CollectionUtilities;

namespace UnitTests
{
	[TestFixture()]
	public class ZipTest
	{		
		[Test()]
		public void ZipWorks()
		{
			List<string> list1 = new List<string>();
			list1.Add("asdf");
			list1.Add("qwerty");
			
			List<int> list2 = new List<int>();
			list2.Add(42);
			list2.Add(100);

			List<string> strings = new List<string>();
			List<int> ints = new List<int>();
			
			foreach (Pair<string, int> values in CollectionUtilities<string, int>.Zip(list1, list2))
			{
				strings.Add(values.first);
				ints.Add(values.second);
			}
			
			Assert.AreEqual(2, strings.Count);
			Assert.AreEqual(2, ints.Count);
			Assert.AreEqual("asdf", strings[0]);
			Assert.AreEqual("qwerty", strings[1]);
			Assert.AreEqual(42, ints[0]);
			Assert.AreEqual(100, ints[1]);
		}
		
		[Test()]
		public void ZipDifferentSizedListsSecondShorter()
		{
			List<string> list1 = new List<string>();
			list1.Add("qwerty");
			list1.Add("quake");
			
			List<int> list2 = new List<int>();
			list2.Add(42);

			List<string> resultStrings = new List<string>();
			List<int> resultInts = new List<int>();
			
			foreach (Pair<string, int> values in CollectionUtilities<string, int>.Zip(list1, list2))
			{
				resultStrings.Add(values.first);
				resultInts.Add(values.second);
			}
			
			Assert.AreEqual(1, resultStrings.Count);
			Assert.AreEqual(1, resultInts.Count);
			Assert.AreEqual("qwerty", resultStrings[0]);
			Assert.AreEqual(42, resultInts[0]);
		}
		
		[Test()]
		public void ZipDifferentSizedListsFirstShorter()
		{
			List<string> list1 = new List<string>();
			list1.Add("asdf");
			
			List<int> list2 = new List<int>();
			list2.Add(42);
			list2.Add(21);
			
			List<string> resultStrings = new List<string>();
			List<int> resultInts = new List<int>();
			
			foreach (Pair<string, int> values in CollectionUtilities<string, int>.Zip(list1, list2))
			{
				resultStrings.Add(values.first);
				resultInts.Add(values.second);
			}
			
			Assert.AreEqual(1, resultStrings.Count);
			Assert.AreEqual(1, resultInts.Count);
			Assert.AreEqual("asdf", resultStrings[0]);
			Assert.AreEqual(42, resultInts[0]);
		}
	}
}
