/*
    Csharp_Utilities - some useful C# utilities
    Copyright (C) 2008 Jonas Granqvist (jonas.granqvist@gmail.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections;
using System.Collections.Generic;

namespace CollectionUtilities
{	
	public class Pair<T, U>
	{
		public T first;
		public U second;
		
		public Pair(T first, U second)
		{
			this.first = first;
			this.second = second;
		}
	}
	
	public static class CollectionUtilities<T, U>
	{
		public static IEnumerable<Pair<T, U>> Zip(ICollection<T> list1, ICollection<U> list2)
		{
			IEnumerator<T> firstEnumerator = list1.GetEnumerator();
			IEnumerator<U> secondEnumerator = list2.GetEnumerator();
			
			while (firstEnumerator.MoveNext() && secondEnumerator.MoveNext())
			{
				yield return new Pair<T, U>(firstEnumerator.Current, secondEnumerator.Current);
			}
		}
	}
}
